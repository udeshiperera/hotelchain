package com.example.hotelchain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelchainApplication {

    public static void main(String[] args) {
        SpringApplication.run(HotelchainApplication.class, args);
    }

}
