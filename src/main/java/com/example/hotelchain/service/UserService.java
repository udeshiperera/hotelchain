package com.example.hotelchain.service;

import com.example.hotelchain.dto.UserDto;
import com.example.hotelchain.entity.UserEntity;

import java.util.List;

public interface UserService {

     List<UserEntity> saveUser(final UserDto userDto);
}
