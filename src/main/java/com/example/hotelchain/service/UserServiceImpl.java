package com.example.hotelchain.service;

import com.example.hotelchain.dto.UserDto;
import com.example.hotelchain.entity.UserEntity;
import com.example.hotelchain.repository.UserRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    private final UserRepo userRepo;

    public UserServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }


    @Override
    public List<UserEntity> saveUser(UserDto userDto) {
        try{
            UserEntity user = new UserEntity();
            return (List<UserEntity>) this.userRepo.save(user);
        }
        catch (Exception e){
            System.out.println(e);
        }
        return null;
    }
}