package com.example.hotelchain.entity;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Data
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "userName")
    @NotNull
    private String userName;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;
}