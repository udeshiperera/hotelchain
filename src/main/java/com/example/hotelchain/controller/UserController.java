package com.example.hotelchain.controller;

import com.example.hotelchain.dto.UserDto;
import com.example.hotelchain.entity.UserEntity;
import com.example.hotelchain.service.UserService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public String root() {
        return "index";
    }

    @GetMapping
    public String showRegistrationForm(Model model) {
        return "registration";
    }

    @PostMapping("/save-user")
    public List<UserEntity> saveUser(@RequestBody UserDto userDto){
        return userService.saveUser(userDto);
    }
}