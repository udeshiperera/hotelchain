package com.example.hotelchain.repository;

import com.example.hotelchain.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface UserRepo extends JpaRepository<UserEntity,Long> {

}
